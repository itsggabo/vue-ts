export default interface UseCase {
    call: () => {
        then: (callback: (result: any) => void) => void;
    }
}