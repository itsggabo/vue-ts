import { defineStore } from "pinia";

export const counterStore = defineStore({
    id: 'counter',
    state: () => ({
        counter: 0
    }),
    actions:{
        increase(){
            this.counter++
        },
        decrease(){
            this.counter--
        }
    }
})