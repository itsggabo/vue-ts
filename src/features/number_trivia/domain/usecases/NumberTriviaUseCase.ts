import UseCase from "../../../../core/usecases/usecase";
import NumberTrivia from "../entities/NumberTrivia";
import NumberTriviaRepository from "../repositories/NumberTriviaRepository";

export default class NumberTriviaUseCase implements UseCase {
    constructor(
        private readonly numberTriviaRepository: NumberTriviaRepository
    ) { }

    public async call(): Promise<NumberTrivia> {
        return this.numberTriviaRepository.getRandomNumberTrivia();
    }
}