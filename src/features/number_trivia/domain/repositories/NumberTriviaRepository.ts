import NumberTrivia from "../entities/NumberTrivia";

export default interface NumberTriviaRepository {
    getRandomNumberTrivia(): NumberTrivia 
}