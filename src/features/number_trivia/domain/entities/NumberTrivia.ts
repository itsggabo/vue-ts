export default class NumberTrivia {
    constructor(
        public text: string,
        public number: number
    ) { }
}