export default class NumberTriviaModel {
    constructor(
        public text: string,
        public number: number
    ) { }
}