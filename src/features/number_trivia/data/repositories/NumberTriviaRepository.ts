import NumberTriviaRepository from "../../domain/repositories/NumberTriviaRepository";
import NumberTrivia from "../../domain/entities/NumberTrivia";

export default class NumberTriviaRepositoryImpl implements NumberTriviaRepository {

    private lorem = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
    
    public getRandomNumberTrivia(): NumberTrivia {
        const random = Math.floor(Math.random() * 100)

        return new NumberTrivia(
            this.lorem.slice(random, random + 50),
            random
        )
    }
}