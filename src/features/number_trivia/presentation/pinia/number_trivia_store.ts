import { defineStore } from "pinia";

interface NumberTrivia {
    text: string, 
    number: number
}

export const triviaStore = defineStore({
    id: 'number_trivia',
    state: () => ({
        NumberTrivia: { text: '', number: 0 } as NumberTrivia,
        lorem: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum" as string
    }),
    actions: {
        getRandomNumber(){
            const random = Math.floor(Math.random() * 100)
            this.NumberTrivia = {
                text: this.lorem.slice(random, random + 50),
                number: random
            }
        }
    }
})