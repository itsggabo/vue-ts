import NotFound from '../pages/not-found.vue'
import Home from '../pages/home.vue'
import CounterPage from '~/features/counter/presentation/pages/index.vue'
import NumberTriviaPage from '~/features/number_trivia/presentation/pages/index.vue'

export const routes = [
    { path: '/:pathMatch(.*)*', name: 'not-found', component: NotFound },
    { 
        path: '/', 
        name: 'home', 
        component: Home, 
        children: [
            {
                path: '/counter',
                name: 'counter',
                component: CounterPage
            },
            {
                path: '/number-trivia',
                name: 'number-trivia',
                component: NumberTriviaPage
            }

        ]
    },
];

