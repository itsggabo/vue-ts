
import { createRouter, createWebHistory } from 'vue-router'
import { routes } from './routes'

export const router = createRouter({
    history: createWebHistory(),
    routes
})

router.resolve({
    name: 'not-found',
    params: { pathMatch: ['not', 'found'] },
}).href