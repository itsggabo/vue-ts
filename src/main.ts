import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { router } from './features/presentation/router'

import ElementPlus from 'element-plus'
import App from './App.vue'
import CustomButton from './features/presentation/components/custom-button.vue'
import Dayjs from 'dayjs'
import 'element-plus/dist/index.css'
import './index.css'


const app = createApp(App)
.use(createPinia())
.use(router)
.use(ElementPlus, { size: 'small', zIndex: 3000 })
app.config.globalProperties.$dayjs = Dayjs
app.component('CustomButton', CustomButton)
app.mount('#app')
